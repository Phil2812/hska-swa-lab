import {Component, OnInit} from '@angular/core';
import {WarehouseService} from '../services/warehouse.service';
import {Warehouse} from '../data/warehouse';
import {WarehouseOrder, WarehouseOrderStatus} from '../data/warehouse-order';
import {ItemsService} from '../services/items.service';
import {Item} from '../data/item';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent implements OnInit {

  isDataAvailable = false;

  items: Item[];
  itemById: { [key: string]: Item } = {};

  warehouse: Warehouse;
  openWarehouseOrders: WarehouseOrder[];
  inProgressWarehouseOrders: WarehouseOrder[];
  canceledWarehouseOrders: WarehouseOrder[];
  deliveredWarehouseOrders: WarehouseOrder[];

  constructor(private warehouseService: WarehouseService,
              private itemsService: ItemsService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.itemsService.getAll().subscribe(items => {
      this.items = items;
      this.items.forEach(item => {
        this.itemById[item.id] = item;
      });

      this.warehouseService.getWarehouseByTechnician(1).subscribe(warehouse => {
        this.warehouse = warehouse[0];
        this.warehouseService.getWarehouseOrdersByStatus(1, WarehouseOrderStatus.New).subscribe(newOrders => {
          this.openWarehouseOrders = newOrders;
          this.warehouseService.getWarehouseOrdersByStatus(1, WarehouseOrderStatus.InProgress).subscribe(inProgressOrders => {
            this.inProgressWarehouseOrders = inProgressOrders;
            this.warehouseService.getWarehouseOrdersByStatus(1, WarehouseOrderStatus.Canceled).subscribe(canceledOrders => {
              this.canceledWarehouseOrders = canceledOrders;
              this.warehouseService.getWarehouseOrdersByStatus(1, WarehouseOrderStatus.Delivered).subscribe(deliveredOrders => {
                this.deliveredWarehouseOrders = deliveredOrders;
                this.isDataAvailable = true;
              });
            });
          });
        });
      });
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
    }, () => {
    });
  }

  createNewOrder(itemId: string, quantity: string, description: string) {
    if (itemId && quantity) {
      const id = 100 + Math.floor(Math.random() * 1000000000);
      const date = new Date().toISOString().split('T')[0]
      const newOrder: WarehouseOrder = {
        id: id,
        technicianId: 1,
        orderNumber: '' + id,
        itemId: itemId,
        quantity: parseFloat(quantity),
        description: description,
        orderDate: date,
        lastUpdate: date,
        status: WarehouseOrderStatus.New
      };
      this.warehouseService.createWarehouseOrder(newOrder).subscribe(() => {
        this.openWarehouseOrders.push(newOrder);
      });
    }
  }

}
