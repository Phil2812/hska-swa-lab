import {Injectable} from '@angular/core';
import {WarehouseOrder} from '../data/warehouse-order';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Warehouse} from '../data/warehouse';

const warehouseEndpoint = environment.endpoint + 'warehouses';
const warehouseOrdersEndpoint = environment.endpoint + 'warehouseOrders';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})

export class WarehouseService {

  constructor(private httpClient: HttpClient) {
  }

  getWarehouseByTechnician(technicianId: number): Observable<Warehouse[]> {
    return this.httpClient.get<Warehouse[]>(warehouseEndpoint + '?technicianId=' + technicianId, httpHeaders);
  }

  getWarehouseOrders(technicianId: number): Observable<WarehouseOrder[]> {
    return this.httpClient.get<WarehouseOrder[]>(warehouseOrdersEndpoint + '?technicianId=' + technicianId, httpHeaders);
  }

  getWarehouseOrdersByStatus(technicianId: number, status: string): Observable<WarehouseOrder[]> {
    return this.httpClient.get<WarehouseOrder[]>(warehouseOrdersEndpoint + '?technicianId=' + technicianId + '&status=' + status,
      httpHeaders);
  }

  createWarehouseOrder(warehouseOrder: WarehouseOrder): Observable<WarehouseOrder> {
    return this.httpClient.post<WarehouseOrder>(warehouseOrdersEndpoint, warehouseOrder, httpHeaders);
  }

}
