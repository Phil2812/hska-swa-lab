export interface ContactInformation {
  phone?: string;
  mail?: string;
  web?: string;
}
