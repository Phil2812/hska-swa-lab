import {Component, OnInit} from '@angular/core';
import {NotesService} from '../services/notes.service';
import {Note} from '../data/note';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  notes: Note[] = []
  constructor(private notesService: NotesService) {
  }

  ngOnInit() {
    this.notesService.getAll().subscribe(notes => this.notes = notes);
  }

}
