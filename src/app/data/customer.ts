import {ContactInformation} from './contact-information';
import {ServiceProduct} from './service-product';
import {ServiceOrder} from './service-order';
import {Geolocation} from './geolocation';

export interface Customer {
  id?: number;
  name?: string;
  geolocation?: Geolocation;
  contact?: ContactInformation;
  address?: string;
  serviceProducts?: ServiceProduct[];
  serviceOrders?: ServiceOrder[];
}
