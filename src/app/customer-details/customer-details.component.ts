import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../services/customers.service';
import {ActivatedRoute, UrlSegment} from '@angular/router';
import {ServiceOrder} from '../data/service-order';
import {Customer} from '../data/customer';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  urlPaths: UrlSegment[] = [];
  customer: Customer = null;
  isDataAvailable = false;

  constructor(private route: ActivatedRoute,
    private customersService: CustomersService) { }

  ngOnInit() {
    this.route.url.subscribe(paths => this.urlPaths = paths);
    const customerId = this.urlPaths[this.urlPaths.length - 1];
    this.customersService.getById(parseInt(customerId.toString(), 10)).subscribe(customer => {
      this.customer = customer;
      this.isDataAvailable = true;
    });
  }

}
