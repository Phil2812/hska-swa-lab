
export enum WarehouseOrderStatus {
  New = 'New',
  InProgress = 'InProgress',
  Canceled = 'Canceled',
  Delivered = 'Delivered'
}

export interface WarehouseOrder {
  id?: number;
  technicianId?: number;
  orderNumber?: string;
  description?: string;
  orderDate?: string;
  lastUpdate?: string;
  status?: WarehouseOrderStatus;
  statusNote?: string;
  itemId?: string;
  quantity: number;
}
