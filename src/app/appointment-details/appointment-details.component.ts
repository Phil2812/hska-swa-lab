import {Component, OnInit} from '@angular/core';
import {ServiceOrderService} from '../services/service-order.services';
import {ActivatedRoute, UrlSegment} from '@angular/router';
import {ServiceOrder, ServiceOrderStatus} from '../data/service-order';
import {Customer} from '../data/customer';
import {ItemsService} from '../services/items.service';
import {Item} from '../data/item';

@Component({
  selector: 'app-appointment-details',
  templateUrl: './appointment-details.component.html',
  styleUrls: ['./appointment-details.component.css']
})
export class AppointmentDetailsComponent implements OnInit {

  urlPaths: UrlSegment[] = [];
  serviceOrder: ServiceOrder = null;
  customer: Customer = null;
  isDataAvailable = false;

  serviceCompletionButtonEnabled = true;
  serviceCompletionMode = false;
  completedServiceOrder: ServiceOrder = null;

  items: Item[];
  itemById: {[key: string]: Item} = {};

  constructor(private route: ActivatedRoute,
              private serviceOrderService: ServiceOrderService,
              private itemsService: ItemsService) {
  }

  ngOnInit() {
    this.route.url.subscribe(paths => this.urlPaths = paths);
    const serviceId = this.urlPaths[this.urlPaths.length - 1];
    this.itemsService.getAll().subscribe(items => {
      this.items = items;
      this.items.forEach(item => {
        this.itemById[item.id] = item;
      });
      this.serviceOrderService.getById(parseInt(serviceId.toString(), 10)).subscribe(serviceOrder => {
        this.serviceOrder = serviceOrder;
        this.isDataAvailable = true;
      });
    });
  }

  completionButtonClick() {
    this.serviceCompletionButtonEnabled = false;
    if (!this.serviceCompletionMode) {
      this.completedServiceOrder = JSON.parse(JSON.stringify(this.serviceOrder)); // deep copy (dirty)
      this.completedServiceOrder.realTime = this.completedServiceOrder.plannedTime;
      this.completedServiceOrder.usedParts = JSON.parse(JSON.stringify(this.completedServiceOrder.plannedParts));
      this.completedServiceOrder.usedServices = JSON.parse(JSON.stringify(this.completedServiceOrder.plannedServices));
      this.completedServiceOrder.status = ServiceOrderStatus.Completed;
      this.serviceCompletionMode = true;
      this.serviceCompletionButtonEnabled = true;
    } else {
      this.serviceOrderService.updateServiceOrder(this.completedServiceOrder.id, this.completedServiceOrder).subscribe(() => {
        this.serviceCompletionMode = false;
        this.serviceOrder = this.completedServiceOrder;
      });
    }
  }

  removeUsedPart(index: number) {
    this.completedServiceOrder.usedParts.splice(index, 1);
  }

  addUsedPart(id: string, quantity: string) {
    if (id && quantity) {
      this.completedServiceOrder.usedParts.push({itemId: id, quantity: parseFloat(quantity)});
    }
  }

  removeUsedService(index: number) {
    this.completedServiceOrder.usedServices.splice(index, 1);
  }

  addUsedService(id: string, quantity: string) {
    if (id && quantity) {
      this.completedServiceOrder.usedServices.push({itemId: id, quantity: parseFloat(quantity)});
    }
  }

}
