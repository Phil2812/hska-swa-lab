import {Technician} from './technician';
import {ServiceProduct} from './service-product';
import {Customer} from './customer';
import {BOMItem} from './bomitem';

export enum ServiceOrderStatus {
  Open = 'Open',
  Completed = 'Completed'
}

export interface ServiceOrder {
  id?: number;
  description?: string;
  technicianId?: number;
  technician?: Technician;
  customer?: Customer;
  customerId?: number;
  serviceProduct?: ServiceProduct;
  serviceProductId?: number;
  plannedParts?: BOMItem[];
  plannedServices?: BOMItem[];
  usedParts?: BOMItem[];
  usedServices?: BOMItem[];
  plannedTime?: number;
  realTime?: number;
  serviceDate?: string;
  status?: ServiceOrderStatus;
}
