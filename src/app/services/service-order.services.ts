import {Injectable} from '@angular/core';
import {ServiceOrder} from '../data/service-order';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Customer} from '../data/customer';

const serviceOrdersEndpoint = environment.endpoint + 'serviceOrders';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})

export class ServiceOrderService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<ServiceOrder[]> {
    return this.httpClient.get<ServiceOrder[]>(serviceOrdersEndpoint);
  }

  /*
  getByStatus(): Observable<ServiceOrder[]> {
  }
  */

  getById(serviceOrderId: number): Observable<ServiceOrder> {
    return this.httpClient.get<ServiceOrder>(serviceOrdersEndpoint + '/' + serviceOrderId, httpHeaders);
  }

  getByStatus(status: String): Observable<ServiceOrder[]> {
    return this.httpClient.get<ServiceOrder[]>(serviceOrdersEndpoint + '?status=' + status, httpHeaders);
  }

  createServiceOrder(serviceOrder: ServiceOrder): Observable<ServiceOrder> {
    return this.httpClient.post<ServiceOrder>(serviceOrdersEndpoint, serviceOrder, httpHeaders);
  }

  updateServiceOrder(id: number, serviceOrder: ServiceOrder): Observable<ServiceOrder> {
    return this.httpClient.put<ServiceOrder>(serviceOrdersEndpoint + '/' + id, serviceOrder, httpHeaders);
  }

  deleteServiceOrder(id: number): Observable<ServiceOrder> {
    return this.httpClient.delete<ServiceOrder>(serviceOrdersEndpoint + '/' + id, httpHeaders);
  }
}
