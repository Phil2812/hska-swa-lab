import { Component, OnInit } from '@angular/core';
import { ServiceOrderService } from '../services/service-order.services';
import {ServiceOrder} from '../data/service-order';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {

  serviceOrdersOpen: ServiceOrder[] = [];
  serviceOrdersCompleted: ServiceOrder[] = [];

  constructor(private serviceOrderService: ServiceOrderService) { }

  ngOnInit() {
    this.serviceOrderService.getByStatus('Open').subscribe(serviceOrders => {this.serviceOrdersOpen = serviceOrders;
    console.log(this.serviceOrdersOpen); });
    this.serviceOrderService.getByStatus('Completed').subscribe(serviceOrders => this.serviceOrdersCompleted = serviceOrders);
  }

}
