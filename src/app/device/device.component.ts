import { Component, OnInit } from '@angular/core';
import {ServiceProductService} from '../services/service-products.service';
import {ActivatedRoute, UrlSegment} from '@angular/router';
import {ServiceProduct} from '../data/service-product';
import {ItemsService} from '../services/items.service';
import {Item} from '../data/item';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  urlPaths: UrlSegment[] = [];
  serviceProduct: ServiceProduct = null;
  isDataAvailable = false;

  items: Item[];
  itemById: {[key: string]: Item} = {};

  constructor(private route: ActivatedRoute,
              private serviceProductService: ServiceProductService,
              private itemsService: ItemsService) {}

  ngOnInit() {
    this.route.url.subscribe(paths => this.urlPaths = paths);
    const serviceProductId = this.urlPaths[this.urlPaths.length - 1];
    this.itemsService.getAll().subscribe(items => {
      this.items = items;
      this.items.forEach(item => {
        this.itemById[item.id] = item;
      });
      this.serviceProductService.getById(serviceProductId.toString()).subscribe(serviceProduct => {
        this.serviceProduct = serviceProduct;
        this.isDataAvailable = true;
      });
    });
  }

}
