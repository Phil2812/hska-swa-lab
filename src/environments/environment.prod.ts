export const environment = {
  production: true,

  endpoint: 'https://swalab.nxcn.org/backend/dev/',
  headers: {
    'Content-Type':  'application/json',
    'Authorization': 'Basic c3dhLWFwaTpzd2FsYWIxODE5'
  }
};
