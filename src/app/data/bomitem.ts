import {Item} from './item';

export interface BOMItem {
  itemId?: String;
  item?: Item;
  quantity?: number;
}
