import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Item} from '../data/item';
import {environment} from '../../environments/environment';
import {Note} from '../data/note';

const itemsEndpoint = environment.endpoint + 'items';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})

export class ItemsService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(itemsEndpoint, httpHeaders);
  }

  createItem(item: Item): Observable<Item> {
    return this.httpClient.post<Item>(itemsEndpoint, item, httpHeaders);
  }

  updateItem(id: String, item: Item): Observable<Item> {
    return this.httpClient.put<Item>(itemsEndpoint + '/' + id, item, httpHeaders);
  }

  deleteItem(id: String): Observable<Item> {
    return this.httpClient.delete<Item>(itemsEndpoint + '/' + id, httpHeaders);
  }
}
