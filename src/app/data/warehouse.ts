import {BOMItem} from './bomitem';

export interface Warehouse {
  technicianId?: number;
  name?: string;
  description?: string;
  parts?: BOMItem[];
}
