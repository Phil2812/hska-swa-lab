import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginComponent } from './login/login.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { NotesComponent } from './notes/notes.component';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { CustomersComponent } from './customers/customers.component';
import { NoteDetailsComponent } from './note-details/note-details.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { DeviceComponent } from './device/device.component';
import { AppointmentDetailsComponent } from './appointment-details/appointment-details.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AppointmentsComponent,
    NotesComponent,
    WarehouseComponent,
    CustomersComponent,
    NoteDetailsComponent,
    CustomerDetailsComponent,
    AppointmentDetailsComponent,
    DeviceComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    NgbModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
