import {Injectable} from '@angular/core';
import {Technician} from '../data/technician';
import {environment} from '../../environments/environment';
import {ServiceProduct} from '../data/service-product';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

const technicianEndpoints = environment.endpoint + 'technicians';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})

export class TechniciansService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Technician[] {
    // Load directly from file??
    return null;
  }

  getById(id: String): Technician {
    return null;
  }

  createTechnician(technician: Technician): Observable<Technician> {
    return this.httpClient.post<Technician>(technicianEndpoints, technician, httpHeaders);
  }

  updateTechnician(id: String, technician: Technician): Observable<Technician> {
    return this.httpClient.put<Technician>(technicianEndpoints + '/' + id, technician, httpHeaders);
  }

  deleteTechnician(id: String): Observable<Technician> {
    return this.httpClient.delete<Technician>(technicianEndpoints + '/' + id, httpHeaders);
  }
}
