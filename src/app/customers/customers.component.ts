import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../services/customers.service';
import {Customer} from '../data/customer';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers: Customer[] = []

  constructor(private customersService: CustomersService) { }

  ngOnInit() {
    this.customersService.getAll().subscribe(customers => this.customers = customers);
  }

}
