import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Note, NoteType} from '../data/note';
import {Technician} from '../data/technician';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

const notesEndpoint = environment.endpoint + 'notes';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})

export class NotesService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(notesEndpoint, httpHeaders);
  }

  getById(id: String): Observable<Note> {
    return this.httpClient.get<Note>(notesEndpoint + '/' + id, httpHeaders);
  }

  createNote(note: Note): Observable<Note> {
    return this.httpClient.post<Note>(notesEndpoint, note, httpHeaders);
  }

  updateNote(id: String, note: Note): Observable<Note> {
    return this.httpClient.put<Note>(notesEndpoint + '/' + id, note, httpHeaders);
  }

  deleteNote(id: String): Observable<Note> {
    console.log('Delete' + id);
    return this.httpClient.delete<Note>(notesEndpoint + '/' + id, httpHeaders);
  }
}
