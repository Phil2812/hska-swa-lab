import {Component, OnInit} from '@angular/core';
import {NotesService} from '../services/notes.service';
import {ActivatedRoute, UrlSegment} from '@angular/router';
import {Note} from '../data/note';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.css']
})
export class NoteDetailsComponent implements OnInit {

  urlPaths: UrlSegment[] = [];
  note: Note = null;
  isDataAvailable = false;

  constructor(private route: ActivatedRoute,
    private notesService: NotesService) {
  }

  ngOnInit() {
    this.route.url.subscribe(paths => this.urlPaths = paths);
    const noteId = this.urlPaths[this.urlPaths.length - 1];
    this.notesService.getById(noteId.toString()).subscribe(note => {
      this.note = note;
      this.isDataAvailable = true;
    });
  }

}
