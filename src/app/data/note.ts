import {Technician} from './technician';

export enum NoteStatus {
  Open = 'Open',
  InProgress = 'InProgress',
  Finished = 'Finished'
}

export enum NoteType {
  Task = 'Task',
  Note = 'Note'
}

export interface Note {
  id?: string;
  title?: string;
  description?: string;
  status?: NoteStatus;
  type?: NoteType;
  creationDate?: string;
  technician?: Technician;
}
