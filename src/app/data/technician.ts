export interface Technician {
  email?: string;
  name?: string;
  password?: string;
  phone?: string;
}
