export interface Item {
  id?: string;
  name?: string;
  unit?: string;
  description?: string;
}
