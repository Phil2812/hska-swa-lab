import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AppointmentsComponent} from '../appointments/appointments.component';
import {CustomersComponent} from '../customers/customers.component';
import {DeviceComponent} from '../device/device.component';
import {LoginComponent} from '../login/login.component';
import {NotesComponent} from '../notes/notes.component';
import {WarehouseComponent} from '../warehouse/warehouse.component';
import {AppointmentDetailsComponent} from '../appointment-details/appointment-details.component';
import {CustomerDetailsComponent} from '../customer-details/customer-details.component';
import {NoteDetailsComponent} from '../note-details/note-details.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/appointments', pathMatch: 'full'},
  {path: 'appointments', component: AppointmentsComponent},
  {path: 'appointments/:appointment', component: AppointmentDetailsComponent},
  {path: 'customers', component: CustomersComponent},
  {path: 'customers/:customer', component: CustomerDetailsComponent},
  {path: 'serviceProducts/:serviceProduct', component: DeviceComponent},
  {path: 'login', component: LoginComponent},
  {path: 'notes', component: NotesComponent},
  {path: 'notes/:note', component: NoteDetailsComponent},
  {path: 'warehouse', component: WarehouseComponent},
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: []
})
export class AppRoutingModule { }
