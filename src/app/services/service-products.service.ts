import {Injectable} from '@angular/core';
import {ServiceProduct} from '../data/service-product';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {ServiceOrder} from '../data/service-order';

const serviceProductsEndpoint = environment.endpoint + 'serviceProducts';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})

export class ServiceProductService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<ServiceProduct[]> {
    return this.httpClient.get<ServiceProduct[]>(serviceProductsEndpoint, httpHeaders);
  }

  getById(id: String): Observable<ServiceProduct> {
    return this.httpClient.get<ServiceProduct>(serviceProductsEndpoint + '/' + id, httpHeaders);
  }

  createServiceOrder(serviceProduct: ServiceProduct): Observable<ServiceProduct> {
    return this.httpClient.post<ServiceProduct>(serviceProductsEndpoint, serviceProduct, httpHeaders);
  }

  updateServiceOrder(id: String, serviceProduct: ServiceProduct): Observable<ServiceProduct> {
    return this.httpClient.put<ServiceProduct>(serviceProductsEndpoint + '/' + id, serviceProduct, httpHeaders);
  }

  deleteServiceOrder(id: String): Observable<ServiceProduct> {
    return this.httpClient.delete<ServiceProduct>(serviceProductsEndpoint + '/' + id, httpHeaders);
  }
}
