import {Injectable} from '@angular/core';
import {Customer} from '../data/customer';
import {Note} from '../data/note';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

const customerEndpoint = environment.endpoint + 'customers';
const httpHeaders = {headers: new HttpHeaders(environment.headers)};

@Injectable({
  providedIn: 'root'
})


export class CustomersService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(customerEndpoint, httpHeaders);
  }

  getById(id: number): Observable<Customer> {
    return this.httpClient.get<Customer>(customerEndpoint + '/' + id, httpHeaders);
  }

  createCustomer(customer: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(customerEndpoint, customer, httpHeaders);
  }

  updateCustomer(id: String, customer: Customer): Observable<Customer> {
    return this.httpClient.put<Customer>(customerEndpoint + '/' + id, customer, httpHeaders);
  }

  deleteCustomer(id: String): Observable<Customer> {
    return this.httpClient.delete<Customer>(customerEndpoint + '/' + id, httpHeaders);
  }
}
