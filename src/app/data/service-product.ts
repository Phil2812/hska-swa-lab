import {Customer} from './customer';
import {BOMItem} from './bomitem';

export interface ServiceProduct {
  id?: number;
  name?: string;
  description?: string;
  customerId?: string;
  customer?: Customer;
  serialNumber?: string;
  purchaseDate?: string;
  documents?: string;
  bom?: BOMItem[];
}
