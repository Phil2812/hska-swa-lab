pipeline {

  environment {
    DEPLOYMENT_NAME = "${env.JOB_NAME}-${env.GIT_COMMIT}"
    ANGULAR_BASE = "/deployment/${DEPLOYMENT_NAME}/hska-fieldapp/"
    COVERAGE_REPORT = "/deployment/${DEPLOYMENT_NAME}/coverage/index.html"
    CHROME_BIN = "chromium"
  }

  agent any

  stages {
    stage('Setup') {
     steps {
        echo 'Setup...'
        sh '''
        npm install
        '''
        sh '''
		    mkdir -p "/var/www/deployment/${DEPLOYMENT_NAME}"
        '''
      }
    }
    stage('Build') {
      steps {
        script {
          echo 'Building...'
          sh '''
          npm run -- ng build --prod --base-href ${ANGULAR_BASE}
          '''
          currentBuild.description = ""
        }
      }
    }
    stage('Test') {
      steps {
        echo 'Unit Testing...'
        script {
          sh '''
          npm run -- ng test
          '''
          echo 'Deploying code coverage report...'
          sh '''
          cp -R coverage "/var/www/deployment/${DEPLOYMENT_NAME}/"
          '''
          currentBuild.description += "<a href='https://swalab.nxcn.org${COVERAGE_REPORT}' target='_blank'>Go to code coverage report</a><br>"
        }
      }
    }
    stage('Deploy') {
      steps {
        echo 'Deploying...'
        script {
          if (env.BRANCH_NAME == 'master') {
            echo 'Job is on the master branch'
          } else {
            echo 'Job is not on the master branch'
          }
          sh '''
          cp -R dist/* "/var/www/deployment/${DEPLOYMENT_NAME}/"
          '''
          currentBuild.description += "<a href='https://swalab.nxcn.org${ANGULAR_BASE}' target='_blank'>Go to deployment</a>"
        }
      }
    }
  }
  post {
    always {
      junit 'test/results.xml'
    }
  }
}
